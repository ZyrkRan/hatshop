package com.coopercraftmc.hatshop.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import com.coopercraftmc.hatshop.Hat;
import com.coopercraftmc.hatshop.HatShop;

public class PlayerListener implements Listener{
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event){
		Player player = (Player) event.getWhoClicked();
		ItemStack item = event.getCurrentItem();
		
		if (!event.getClickedInventory().equals(HatShop.getShopManager().getShopGUI())){
			return;
		}
		
		// Check if click item is valid
		if (item == null || item.getType().equals(Material.AIR)) {
			return;
		}
		
		String name = HatShop.decolor(item.getItemMeta().getDisplayName());
		event.setCancelled(true);
		
		for (Hat hat : HatShop.getHatManager().getLoadedHats()){
			if (name.equalsIgnoreCase(HatShop.decolor(hat.getName()))){
				Bukkit.getPluginManager().callEvent(new PlayerRequestsPurchaseEvent(player, hat));
				System.out.println("Called event with " + hat.getName());
				return;
			}
		}
	}
}
