package com.coopercraftmc.hatshop.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import com.coopercraftmc.hatshop.Hat;
import com.coopercraftmc.hatshop.HatShop;

public class PurchaseListener implements Listener{
	
	@EventHandler
	public void onPurchaseRequest(PlayerRequestsPurchaseEvent event){
		Player player = event.getPlayer();
		Hat hat = event.getHat();
		
		boolean isEmpty = false;
		for (ItemStack i : player.getInventory().getContents()){
			if (i == null){
				isEmpty = true;
			}
		}
		
		// User's inventory is full
		if (!isEmpty){
			player.sendMessage(HatShop.color("&cPlease make some space in your inventory before you purchase a hat!"));
			return;
		}
		
		// Check if user has enough money, if not send them an error message
		if (HatShop.getEconomy().has(player, hat.getPrice())){
			HatShop.getEconomy().withdrawPlayer(player, hat.getPrice());

			Hat hat_item = HatShop.getHatManager().getHat(event.getHat().getStringID());
			
			// Give player a new instance of purchased hat
			player.getInventory().addItem(hat_item.toItemStack(false));
			player.sendMessage(HatShop.color("&aYou have purchased a " + hat_item.getName()));
			return;
		} else {
			player.sendMessage(HatShop.color("&cYou don't have enough money to purchase a hat"));
			return;
		}
	}
}
