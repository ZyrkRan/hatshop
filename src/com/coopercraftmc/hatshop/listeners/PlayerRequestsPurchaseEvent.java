package com.coopercraftmc.hatshop.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.coopercraftmc.hatshop.Hat;

public class PlayerRequestsPurchaseEvent extends Event{
	
	private Player player;
	private Hat hat;
	
	public PlayerRequestsPurchaseEvent(Player player, Hat hat){
		this.player = player;
		this.hat = hat;
	}
	
	public Player getPlayer(){
		return player;
	}
	
	public Hat getHat(){
		return hat;
	}
	
	/****************************************
	 * Spigot event crap xd (Do not edit)   *
	 ****************************************/
	
	private static final HandlerList handlers = new HandlerList();
    
    public HandlerList getHandlers() {
        return handlers;
    }
     
    public static HandlerList getHandlerList() {
        return handlers;
    }

}
