package com.coopercraftmc.hatshop;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Item {
	
	private ItemStack item;
	private ItemMeta meta;
	
	private HatInfo info;
	
	public Item(Material material, int data){
		item = new ItemStack(material, 1, (short)data);
		meta = item.getItemMeta();
	}
	
	public Item applyItemMeta(HatInfo info){
		this.info = info;
		
		// Set item damage level invisible
		meta.spigot().setUnbreakable(true);
		meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ATTRIBUTES);

		// Change item's name and lore
		setName(info.getName());
		setLore(info.getLore());
		
		item.setItemMeta(meta);
		return this;
	}
	
	public Item applyItemMetaWithoutLore(HatInfo info){
		this.info = info;
		
		// Set item damage level invisible
		meta.spigot().setUnbreakable(true);
		meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ATTRIBUTES);
		
		// Change item's name
		setName(info.getName());
		
		item.setItemMeta(meta);
		return this;
	}
	
	public Item hideDamage(){
		return this;
	}
	
	public ItemStack getItemStack(){
		return item;
	}
	
	private void setName(String name){
		name = HatShop.color(name);
		meta.setDisplayName(name);
	}
	
	private void setLore(List<String> lore){
		for (int i=0; i<lore.size(); i++){
			lore.set(i, HatShop.color(lore.get(i)));
			
			// Replace placeholder with the actual price
			if (lore.get(i).contains("{PRICE}")){
				lore.set(i, lore.get(i).replace("{PRICE}", info.getPrice() + ""));
			}
		}
		meta.setLore(lore);
	}
}
