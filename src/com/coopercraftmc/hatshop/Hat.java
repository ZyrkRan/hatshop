package com.coopercraftmc.hatshop;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Hat {
	
	private String id; 				// configuration file id
	private String name;			// item display name
	private List<String> lore;		// item lore while on the shop
	
	private int data;				// this number will change the type of hat [1-6]
	private double price;			// how much is worth the hat
	
	public Hat(String id){
		this.id = id;
		this.name  =  HatShop.color(HatShop.getInstance().getConfig().getString("hats-config." + id + ".name"));
		this.lore  =  HatShop.getInstance().getConfig().getStringList("hats-config." + id + ".lore");
		this.price =  HatShop.getInstance().getConfig().getInt("hats-config." + id + ".price");
		this.data  =  HatShop.getInstance().getConfig().getInt("hats-config." + id + ".data");
	}
	
	public String getStringID(){
		return id;
	}
	
	public String getName(){
		return name;
	}
	
	public List<String> getLore(){
		return lore;
	}
	
	public int getData(){
		return data;
	}
	
	public double getPrice(){
		return price;
	}
	
	public ItemStack toItemStack(boolean withLore){
		ItemStack item = new ItemStack(Material.DIAMOND_HOE, 1, (short)data);
		ItemMeta meta = item.getItemMeta();

		meta.spigot().setUnbreakable(true);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE);
		
		meta.setDisplayName(HatShop.color(name));
		
		if (withLore){
			for (int i=0; i<lore.size(); i++){
				lore.set(i, HatShop.color(lore.get(i)));
				
				// Replace placeholder with the actual price
				if (lore.get(i).contains("{PRICE}")){
					lore.set(i, lore.get(i).replace("{PRICE}", getPrice() + ""));
				}
				meta.setLore(lore);
			}
		}
		
		item.setItemMeta(meta);
		
		return item;
	}

}
