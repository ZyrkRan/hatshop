package com.coopercraftmc.hatshop.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.coopercraftmc.hatshop.Hat;
import com.coopercraftmc.hatshop.HatShop;

import net.md_5.bungee.api.ChatColor;

public class CommandHat implements CommandExecutor{
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (!(sender instanceof Player)){
			sender.sendMessage(ChatColor.RED + "This commands is only for in-game players!");
			return false;
		}
		
		Player player = (Player) sender;
		
		if (args.length == 0){
			HatShop.getShopManager().openGUI(player);
			return true;
		}
		
		else if (args.length == 1){
			if (args[0].equalsIgnoreCase("wear")){
				if (player.getInventory().getItemInMainHand().getType() == null || player.getInventory().getItemInMainHand().getType() == Material.AIR){
					player.sendMessage(ChatColor.RED + "You're not holding a hat!");
					return false;
				}
				
				for (Hat hat : HatShop.getHatManager().getLoadedHats()){
					String itemname = HatShop.decolor(player.getInventory().getItemInMainHand().getItemMeta().getDisplayName());
					if (itemname.equalsIgnoreCase(HatShop.decolor(hat.getName())) && player.getInventory().getItemInMainHand().getType().equals(Material.DIAMOND_HOE)){
						player.getInventory().setHelmet(hat.toItemStack(false));
						
						if (player.getInventory().getItemInMainHand().getAmount() > 1){
							player.getInventory().getItemInMainHand().setAmount(player.getInventory().getItemInMainHand().getAmount()-1);
						} else {
							player.getInventory().remove(player.getInventory().getItemInMainHand());
						}
						player.updateInventory();
						player.sendMessage(ChatColor.GREEN + "You are now wearing " + hat.getName());
						return true;
					}
				}
				player.sendMessage(ChatColor.RED + "This is not a hat!");
				return false;
			}
			
			else if (args[0].equalsIgnoreCase("reload")){
				if (player.hasPermission("hatshop.admin") || player.isOp()){
					HatShop.reloadConfiguration();
					player.sendMessage(ChatColor.GREEN + "Configuration file has been reloaded!");
					return true;
				} else {
					player.sendMessage(ChatColor.RED + "You don't have permission to do that!");
					return false;
				}
				
			}
			
			else {
				player.sendMessage(ChatColor.RED + "Wrong command");
				return false;
			}
		}
		
		player.sendMessage(ChatColor.RED + "Too many arguments!");
		return false;
	}

}
