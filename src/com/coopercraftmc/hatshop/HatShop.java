package com.coopercraftmc.hatshop;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.coopercraftmc.hatshop.commands.CommandHat;
import com.coopercraftmc.hatshop.gui.ShopManager;
import com.coopercraftmc.hatshop.listeners.PlayerListener;
import com.coopercraftmc.hatshop.listeners.PurchaseListener;

import net.milkbowl.vault.Vault;
import net.milkbowl.vault.economy.Economy;

/**
 * HatShop is a plugin for Spigot 1.10.2 that allows players to purchase different custom hats using 3D Models
 * and durability features added in Minecraft 1.9
 * 
 * @author ZyrkRan
 *
 */
public class HatShop extends JavaPlugin{
	
	private static HatShop instance;
	
	private static ShopManager shopManager;
	private static HatManager hatManager;
	
	private static Vault vault;
	private static Economy economy;
	
	public void onEnable(){
		instance = this;
		
		PluginManager pm = Bukkit.getPluginManager();
		
		// hook into vault (economy setup)
		vault = (Vault) pm.getPlugin("Vault");
		if (vault != null){
			RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
			if (rsp != null){
				economy = rsp.getProvider();
			}
		}
		
		hatManager = new HatManager();
		shopManager = new ShopManager();
		
		// register events
		pm.registerEvents(new PurchaseListener(), this);
		pm.registerEvents(new PlayerListener(), this);
		
		// register commands
		getCommand("hats").setExecutor(new CommandHat());
		
		// load configuration file
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	
	public static String color(String string){
		string = ChatColor.translateAlternateColorCodes('&', string);
		return string;
	}
	
	public static String decolor(String string){
		string = ChatColor.stripColor(string);
		return string;
	}
	
	public static HatShop getInstance(){
		return instance;
	}
	
	public static ShopManager getShopManager(){
		return shopManager;
	}
	
	public static HatManager getHatManager(){
		return hatManager;
	}
	
	public static Economy getEconomy(){
		return economy;
	}
	
	public static void reloadConfiguration(){
		HatShop.getInstance().reloadConfig();
	}

}
