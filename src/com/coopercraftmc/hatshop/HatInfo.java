package com.coopercraftmc.hatshop;

import java.util.List;

public enum HatInfo {
	
	BLACK_TOP_HAT("black-top-hat"),
	BONNET("bonnet"),
	GREEN_HAT("green-hat"),
	PIRATE_HAT("pirate-hat"),
	COWBOY_HAT("cowboy-hat"),
	ANDY_CAP("andy-cap");
	
	private String name;
	private List<String> lore;
	
	private int price;
	
	HatInfo(String name){
		this.name = HatShop.getInstance().getConfig().getString("hats-config." + name + ".name");
		this.lore = HatShop.getInstance().getConfig().getStringList("hats-config." + name + ".lore");
		this.price = HatShop.getInstance().getConfig().getInt("hats-config." + name + ".price");
	}
	
	public String getName(){
		return name;
	}
	
	public List<String> getLore(){
		return lore;
	}
	
	public int getPrice(){
		return price;
	}
	
	public static HatInfo infoFrom(String id){
		HatInfo a = null;
		for (HatInfo i : HatInfo.values()){
			if (id.equalsIgnoreCase(i.getName())){
				a = i;
			}
		}
		return a;
	}

}
