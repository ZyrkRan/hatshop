package com.coopercraftmc.hatshop;

import java.util.ArrayList;
import java.util.List;

public class HatManager {
	
	public List<Hat> loadedHats;
	
	public HatManager(){
		loadedHats = new ArrayList<Hat>();

		// load hats in config
		for (String s : HatShop.getInstance().getConfig().getConfigurationSection("hats-config").getKeys(false)){
			Hat hat = new Hat(s);
			loadedHats.add(hat);
		}
	}
	
	public List<Hat> getLoadedHats(){
		return loadedHats;
	}
	
	public Hat getHat(String id){
		for (Hat hat : loadedHats){
			if (hat.getStringID().equalsIgnoreCase(id)) return hat;
		}
		return null;
	}
}
