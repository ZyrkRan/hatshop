package com.coopercraftmc.hatshop.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.coopercraftmc.hatshop.Hat;
import com.coopercraftmc.hatshop.HatShop;
import com.coopercraftmc.hatshop.Item;

public class ShopManager {
	
	private Inventory inventory;
	
	public ShopManager(){
		setup();
	}
	
	public Inventory getShopGUI(){
		return inventory;
	}
	
	public void openGUI(Player player){
		player.openInventory(getShopGUI());
	}

	private void setup(){
		inventory = Bukkit.createInventory(null, 9, "HatShop Menu");
		
		for (Hat hat : HatShop.getHatManager().getLoadedHats()){
			getShopGUI().addItem(hat.toItemStack(true));
		}

		getShopGUI().setItem(8, new Item(Material.STAINED_GLASS_PANE, (short)7).getItemStack());
		getShopGUI().setItem(7, new Item(Material.STAINED_GLASS_PANE, (short)7).getItemStack());
		getShopGUI().setItem(6, new Item(Material.STAINED_GLASS_PANE, (short)7).getItemStack());
	}
}
